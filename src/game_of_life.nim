import random, os, terminal

const
    height = 40
    width = 60

var
    world: seq[seq[bool]]

proc ctrlc() {.noconv.} =
    showCursor()
    quit(130)

proc randBool(): bool =
    randomize()
    var state = rand(1)
    if state == 1:
        result = true

proc randWorld(height, width: int): seq[seq[bool]] =
    for i in 0..<height:
        var cells: seq[bool]
        for j in 0..<width:
            cells.add(randBool())
        result.add(cells)

proc deadWorld(height, width: int): seq[seq[bool]] =
    for i in 0..<height:
        var cells: seq[bool]
        for j in 0..<width:
            cells.add(false)
        result.add(cells)

proc render(w: seq[seq[bool]]) =
    stdout.eraseScreen()
    for x, cells in w:
        for y, state in cells:
            #setCursorPos(stdout, x, y)
            setCursorPos(stdout, y, x)
            if state:
                stdout.write("•")
            else:
                stdout.write(" ")
        stdout.write("\n")
    stdout.flushfile

proc nextState(w: seq[seq[bool]]): seq[seq[bool]] =
    result = deadWorld(len(w), len(w[0]))

    var population: int
    for x, cells in w:
        for y, state in cells:
            population = 0
            for yi in y-1..y+1:
                for xi in x-1..x+1:
                    if w[(xi+height) mod height][(yi+width) mod width]:
                        population += 1
            if w[x][y]:
                population -= 1

            if state:
                case population:
                of 0..1:
                    result[x][y] = false
                of 2..3:
                    result[x][y] = true
                of 4..8:
                    result[x][y] = false
                else: discard
            else:
                if population == 3:
                    result[x][y] = true

proc main() =
    hideCursor()
    setControlCHook(ctrlc)

    world = randWorld(height, width)
    while true:
        world.render()
        world = nextState(world)
        sleep(300)

main()
