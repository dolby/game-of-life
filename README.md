# Conway's Game of Life

My learning project for [Nim](https://nim-lang.org/).

## TODO

* Figure out how to run unittests, and add more.
* Add runtime flags.
* Better output rendering.
