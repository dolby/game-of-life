# Package

version       = "0.1.0"
author        = "Andrew Dolby"
description   = "Conway's Game of Life"
license       = "GPL-3.0"
srcDir        = "src"
bin           = @["game_of_life"]



# Dependencies

requires "nim >= 1.0.6"
