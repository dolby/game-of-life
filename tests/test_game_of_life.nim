import unittest

include game_of_life

suite "Life Rules":
    setup:
        var
            currentState: seq[seq[int]]
            startState1 = @[
                @[0, 0, 0],
                @[0, 0, 0],
                @[0, 0, 0],
            ]
            expectState1 = @[
                @[0, 0, 0],
                @[0, 0, 0],
                @[0, 0, 0],
            ]
            startState2 = @[
                @[0, 0, 1],
                @[0, 1, 1],
                @[0, 0, 0],
            ]
            expectState2 = @[
                @[0, 1, 1],
                @[0, 1, 1],
                @[0, 0, 0],
            ]

    test "1. Dead cells with no live neigbours stay dead":
        currentState = nextState(startState1, 3, 3)
        check currentState == expectState1

    test "2. Dead cells with exactly 3 neigbours should come alive":
        currentState = nextState(startState2, 3, 3)
        check currentState == expectState2
